# [logika.info](https://logika.info) source codes

<br/>

### Run logika.info on localhost

    # vi /etc/systemd/system/logika.info.service

Insert code from logika.info.service

    # systemctl enable logika.info.service
    # systemctl start logika.info.service
    # systemctl status logika.info.service

http://localhost:4048
